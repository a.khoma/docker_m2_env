FROM mgtcommerce/mgt-development-environment-7.0

LABEL 2kish "<2kishh@gmail.com>"

#RUN apt-get update; exit 0;

RUN apt-get install -y build-essential software-properties-common ruby ruby-dev libsqlite3-dev

RUN gem install mailcatcher --no-ri --no-rdoc

RUN mailcatcher

COPY files/etc/postfix etc/postfix

RUN /etc/init.d/postfix restart

COPY files/etc/nginx/sites-enabled /etc/nginx/sites-enabled

RUN sudo /etc/init.d/nginx restart

